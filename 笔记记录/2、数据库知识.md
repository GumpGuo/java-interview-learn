## 1、 MVCC
1. MVCC是什么：多版本并发控制
2. MVCC如何实现的：依赖于隐藏字段、Read View、undo log（事务回滚日志）。
	1. 隐藏字段：
		1. DB_TRX_ID:表示最后一次更新或插入该行的事务.
		2. DB_ROLL_PTR:回滚指针。指向该行undo log
		3. DB_ROW_ID：如果没有设置主键切没有位移非控索引是。用该ID生成聚组索引。
	2. ReadView
		1.  m_low_limit_id: 目前出现的最大的事务ID+1。大于等于这个ID的数据版本均不可见。
		2.  m_up_id:活跃事务列表中最小的事务ID。小于等于这个事务ID的数据版本均可见
		3.  m_ids：创建事务时其他未提交的活跃事务ID列表。
		4.  m_create_trx_id:创建该Read View的事务ID。
	3. undo-log
		1. 两个作用：
			1. 当事务回滚时将叔恢复到修改前的样子。
			2. 另一个作用是MVCC，当读取事务时，若该记录被其他事务占用或者当前版本对该事务不可见，则可以通过undo-log读取之前的版本数据。以此实现非锁定读。
	
## 2、数据库三范式
 - 第一范式：属性不可分割。
 - 第二范式： 非主属性都依赖于主属性。
 - 第三范式： 属性之间的传递依赖关系。

## 3、事务的隔离级别 ##
1. 脏读：读取了其他事务未提交的数据
2. 不可重复读： 读取了其他事务修改完的数据。 
3. 幻读：事务执行过程中。其他事务插入或者删除了数据
4. 事务的隔离级别
	1. 读未提交
	2. 读已提交
	3. 可重读
	4. 线性化
5. 事务的ACID原则：
	1. 原子性：事务是最小的执行单位，不允许分割
	2. 一致性：事务执行前后，数据保持一致性
	3. 隔离性：事务之间不会相互影响
	4. 持久性：事务提交会，对数据的修改是永久的。redo-log.

## 4、MySQL数据库的基础架构
 - 连接器：身份认证和权限相关（登入数据库的时候）
 - 查询缓存：执行SQL语句之前会先查询缓存（MySQL 8.0 版本被移除，不实用）
 - 分析器：分析SQL语句，检查SQL语句语法是否正确
 - 优化器：按照MySQL认为最优的方案去优化语句
 - 执行器：执行语句，然后存储引擎返回数据
 - 存储引擎：主要负责数据存储和读取，采用的是插件式结构

## 5、什么是关系型数据库？
 - 建立在关系模型基础上的数据库，关系模型表明了数据库中所存储的数据之间的关系。

## 6、什么是SQL语句？
 - 结构化查询语句，专门用于和关系型数据库打交道。

## 7、MySQL的查询缓存？	
 - 开启缓存查询后在同样查询条件以及数据的基础上，会直接在缓存中返回结果。这里的查询条件包括查询本身、查询的数据库、客户端的协议版本号等一些可能影响结果的信息。
 - 缓存不命中的情况：
	- 任何两个查询在任何字符上的不同都会导致缓存不命中。
	- 查询中包含自定义函数、存储函数、用户变量、临时表、MySQL中的系统表。其查询结果都不会被缓存。
	- 缓存建立以后，MySQL会跟踪涉及到的每张表，如果这些表的数据发生变换。那么这张表相关的所有缓存都会失效。
	
## 8、什么是事务？
 - 事务就是逻辑上的一组动作，要么都执行成功，要么都不执行。

## 9、并发事务带来哪些问题？
 - 脏读
 - 丢失修改
 - 不可重复度
 - 幻读

## 10、并发事务控制的方式哪些？
 - 锁：锁控制方式下会通过锁来显示控制资源而不是通过调度手段，MySQL中主要通过读写锁来实现并发控制。
   	- 共享锁：又称读锁，事务在读物数据的时候获取读锁，允许多个事务同时获取
    - 排他锁：又称写锁，事务在修改记录时获取排他锁，不允许多个事务之间获取排他锁。如果一个记录已经被加了排他锁，那么事务不能再对这条记录加任何类型的锁。
 - MVCC多版本并发控制：即对一份数据会存储多个版本。通过事务的可见性来保证事务能看到自己改看的版本，

## 11、InnoDB 有哪几类行锁？
 - 记录锁（Record Lock）：属于当个行记录上的锁
 - 间隙锁（Gap Lock）：锁定一个范围，不包括记录本身
 - 临建锁（Next-Key Lock）： 锁定一个范围，包含记录本身，主要问题是解决幻读。

## 12、共享锁和排他锁？
 - 共享锁（S锁）：又称读锁，事务在读取记录的时候获取共享锁，允许多个事务同时获取（锁兼容）。
 - 排他锁（X锁）：又称写锁/独占锁，事务在修改记录的时候获取排他锁，不允许多个事务同时获取。如果一个记录已经被加了排他锁，那其他事务不能再对这条事务加任何类型的锁（锁不兼容）。

## 13、意向锁？
 - 意向锁：如果需要用到表锁的话，如何判断表中的记录没有行锁呢，一行一行遍历肯定是不行，性能太差。我们需要用到一个叫做意向锁的东东来快速判断是否可以对某个表使用表锁。
	- 意向共享锁：事务有意向对表中的某些记录加共享锁（S 锁），加共享锁前必须先取得该表的 IS 锁。
	- 意向排他锁：事务有意向对表中的某些记录加排他锁（X 锁），加排他锁之前必须先取得该表的 IX 锁。
	
## 14、当前读和快照读有什么区别？
 - 快照读（一致性非锁定读）就是单纯的select语句。
 - 快照即记录的历史版本。
 - 快照读的情况下，如果读取的记录正在执行update/Delete操作，读取操作不会因此去等待记录上X锁释放，而是回去读取行的一个快照。
 - 只有事务隔离级别在RC、RR情况下，Innodb才会使用一致性非锁定读
 	- 在RC级别下，对于快照读，一致性非锁定读写总是读取被锁定行的最新一份快照数据
	- RR级别下，对于快照读，一致性非锁定度总是读物事务开始时的最新一份快照数据

## 16、什么是索引？
 - 索引是一种用于快速查询和检索的特殊的数据结构，其本质可以看成是一种排序好的数据结构。

## 17、索引的优缺点：
 - 优点：
   	- 使用索引可以大大减少查询时间。
 	- 使用唯一索引可以确保数据的唯一性。
 	
 - 缺点：
    - 索引需要占用磁盘空间
	- 新建索引需要时间
	- 增删改查操作都需要维护索引

## 18、索引底层数据结构
 - Hash
 - 二叉查找叔
 - AVL树
 - 红黑树
 - B树和B+树
	 - B树也成 B-树，全称为多路平衡查找树，B+树是B树的一种变体。 
	 - B树和B+树有什么异同？
		- B树的所有节点既存放Key，也存放数据，而B+树只有叶子节点存放key和data，其他内节点只存Key、
	    - B树的叶子节点都是独立的，B+树的叶子节点有一条引用链条执行他相邻的叶子节点。
	    - B树的检索过程相当于范围内的每个节点的关键字做二分查找，可能还没到到叶子节点就已经结束了，而B+树就很稳定，任何查找都是从根节点到叶子节点的过程。
	    - B树在范围查询是，首先需要找打需要查询的下限，然后对B树进行中序遍历，直到直到上限。而B+树的范围查询，只要对链表遍历即可。

## 19、索引的分类
 - 按照数据结构分类
  - hash
  - B+树
 - 按照底层存储方式
  - 聚簇索引：索引结构和数据存放在一起的。
	- 优点： 查询速度快 ；对排序和查找范围优化
	- 缺点：依赖于有序的数据 ；更新代价大。
  - 非聚簇索引：索引结构和数据不存放在一起的。二级索引就是非聚簇索引。
	- 优点：更新代价小
	- 缺点：依赖于有序的数据；可能会二次查询
 - 按照应用角度划分
  - 主键索引
  - 唯一索引
  - 普通索引
  - 覆盖索引
  - 联合索引
  - 全文索引

## 20、联合索引
 - 使用表中多个字段创建索引

## 21、最左前缀匹配原则
 - 最左前缀匹配原则指的是，在使用联合索引时，MySQL 会根据联合索引中的字段顺序，从左到右依次到查询条件中去匹配，如果查询条件中存在与联合索引中最左侧字段相匹配的字段，则就会使用该字段过滤一批数据，直至联合索引中全部字段匹配完成，或者在执行过程中遇到范围查询（如 >、<）才会停止匹配。对于 >=、<=、BETWEEN、like 前缀匹配的范围查询，并不会停止匹配。所以，我们在使用联合索引时，可以将区分度高的字段放在最左边，这也可以过滤更多数据。

## 22、索引下推？
 - 索引下推（Index Condition Pushdown） 是 MySQL 5.6 版本中提供的一项索引优化功能，可以在非聚簇索引遍历过程中，对索引中包含的字段先做判断，过滤掉不符合条件的记录，减少回表次数。

##23、MySQL的日志？
 - redo log（重做日志），是InnoDB独有的，他让MySQL有了崩溃恢复能力。
 - bin log ：物理日志，记录内容是“在某个数据页上做了什么修改”，记录内容是语句的原始逻辑。
   	- 用于数据备份、主备、主主、主从离不来bin log
 - undo log	
	- 我们知道如果想要保证事务的原子性，就需要在异常发生时，对已经执行的操作进行回滚，在 MySQL 中，恢复机制是通过 回滚日志（undo log） 实现的，所有事务进行的修改都会先记录到这个回滚日志中，然后再执行相关的操作。如果执行过程中遇到异常的话，我们直接利用 回滚日志 中的信息将数据回滚到修改之前的样子即可！并且，回滚日志会先于数据持久化到磁盘上。这样就保证了即使遇到数据库突然宕机等情况，当用户再次启动数据库的时候，数据库还能够通过查询回滚日志来回滚将之前未完成的事务。
	- 另外，MVCC 的实现依赖于：隐藏字段、Read View、undo log。在内部实现中，InnoDB 通过数据行的 DB_TRX_ID 和 Read View 来判断数据的可见性，如不可见，则通过数据行的 DB_ROLL_PTR 找到 undo log 中的历史版本。每个事务读到的数据版本可能是不一样的，在同一个事务中，用户只能看到该事务创建 Read View 之前已经提交的修改和该事务本身做的修改# 总结
	
## 24、redo-log 两阶段提交
 - 原理很简单：将redo log拆分成了两个步骤 prepare 和 commit

## 25、MVCC+Next-key-Lock 防止换读。
 - 执行普通select，此时会议MVCC快照读的方式读取数据。
 - 执行当前读：在当前读下，读取的都是当前最新的数据，如果其他事务有插入新的记录，并且刚好在当前事务的查询范围内，就会产生幻读。InnoDB使用Next-Key Lock来防止这种情况。当执行当前读时，会锁定读取到的记录的同时，锁定它们的间隙，防止其它事务在查询范围内插入数据。只要我不让你插入，就不会发生幻读

## 26、InnoDB为什么使用B+树索引？
 - 哈希索引虽然能提供O（1）复杂度查询，但对范围查询和排序却无法很好的支持，最终会导致全表扫描。
 - B 树能够在非叶子节点存储数据，但会导致在查询连续数据可能带来更多的随机 IO。
 - 而 B+ 树的所有叶节点可以通过指针来相互连接，减少顺序遍历带来的随机 IO。

## 27、 drop、delete、truncate。？
 - delete：DELETE 语句执行删除的过程是每次从表中删除一行，并且同时将该行的删除操作作为事务记录在日志中保存以便进行进行回滚操作。
 - TRUNCATE TABLE：则一次性地从表中删除所有的数据并不把单独的删除操作记录记入日志保存，删除行是不能恢复的。并且在删除的过程中不会激活与表有关的删除触发器。执行速度快。
 - drop语句将表所占用的空间全释放掉。
  - 在速度上，一般来说，drop> truncate > delete。
  - 如果想删除部分数据用 delete，注意带上 where 子句，回滚段要足够大；
  - 如果想删除表，当然用 drop； 如果想保留表而将所有数据删除，如果和事务无关，用 truncate 即可；

